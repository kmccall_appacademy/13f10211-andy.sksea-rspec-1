def add(n, n1)
  n + n1
end

def subtract(n, n1)
  n - n1
end

def sum(arr)
  arr.reduce(0, :+)
end

def multiply(*args)
  if args[0].class == Array
    return 0 if args[0].empty?
    args[0].reduce(:*)
  else 
    args[0] * args[1]
  end
end

def power(n, pow)
  ([n]*pow).reduce(:*)
end

def factorial(n)
  return 1 if n == 0 || n == 1
  [*2..n].reduce(:*)
end