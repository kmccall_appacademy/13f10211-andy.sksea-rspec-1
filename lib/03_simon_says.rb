def echo(str)
  str
end

def shout(str)
  str.upcase
end

def repeat(str, times=2)
  ([str]*times).join(" ")
end

def start_of_word(word, n)
  word[0..n-1]
end

def first_word(str)
  str.split(" ")[0]
end

def titleize(str)
  little_words = ['and', 'of', 'the', 'over']
  str.split(" ").map.with_index{|w, i| (i == 0 || !little_words.include?(w)) ? w.capitalize : w}.join(" ")
end

