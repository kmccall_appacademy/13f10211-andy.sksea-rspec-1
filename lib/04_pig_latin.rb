# not sure why this function errors when loading into irb/pry.
# NoMethodError: undefined method `split' for #<RubyVM::InstructionSequence:0x007fcabc8cf4b8>

def translate(str)
  vowels = "aieouAEIOU"
  accepted_phonemes = ["qu"].join("|")
  words = str.split(" ")
  words.map do |w|
    if vowels.include?(w[0])  
      w + "ay"
    else
      # splits word using regex capture groups
      fragments = w.scan(/([^#{vowels}]*#{accepted_phonemes}|[^#{vowels}]*)(.*)/)[0]
      fragments[1] + fragments[0] + "ay"
    end
  end.join(" ")
end
